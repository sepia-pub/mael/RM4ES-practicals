{ 
 kapack ? import
    (fetchTarball "https://github.com/oar-team/nur-kapack/archive/901a5b656f695f2c82d17c091a55db2318ed3f39.tar.gz")
    {}
, pkgs ? kapack.pkgs
, python3 ? pkgs.python3
, python3Packages ? pkgs.python3Packages
}:

let
  jobs = rec {
    evalys = kapack.evalys.overrideAttrs (attr: rec {
      name = "evalys";
      version = "d4d47bd2f4d076730b05f1345b1bf9032cd28753";
      src = pkgs.fetchgit rec {
        url = "https://github.com/Mema5/evalys.git";
        rev = version;
        sha256 = "sha256-eHGRNj2xWBoEgG2wvAZUoH1Us5d1yu9emu1Zyyms1JU=";
      };
    });

    expe = pkgs.mkShell rec {
      buildInputs = [
        kapack.batsim-410
        kapack.pybatsim-320
        python3Packages.matplotlib
        evalys
      ];
      QT_QPA_PLATFORM_PLUGIN_PATH=  with pkgs.libsForQt5.qt5; "${qtbase.bin}/lib/qt-${qtbase.version}/plugins/platforms";      
    };
  };

in
  jobs
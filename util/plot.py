from evalys.jobset import JobSet
import matplotlib.pyplot as plt
import argparse


def plot(input, output, noDisplay):
    js = JobSet.from_csv(input)
    js.plot(with_details=True)
    if output is not None:
        plt.savefig(output)
    if not noDisplay:
        plt.show()


def main():
    parser = argparse.ArgumentParser(
    description="Reads a SWF (Standard Workload Format) file and transform "
                "it into a JSON Batsim workload (with delay jobs)") 

    parser.add_argument('input_csv', type=str,
                        help='The input jobs.csv file')
    parser.add_argument('-o', '--output_pdf', type=str, default=None,
                        help='The output PDF file') 
    parser.add_argument('--noDisplay', action="store_true",
                        help='If set, will not pop-up the graph') 

    args = parser.parse_args()
    plot(args.input_csv, args.output_pdf, args.noDisplay)

if __name__ == "__main__":
    main()
    


"""
Trivial example scheduler that rejects any job.
"""

from batsim.batsim import BatsimScheduler, Batsim


class Rejector(BatsimScheduler):
    def onJobSubmission(self, job):
        self.bs.reject_jobs([job])  # nope!

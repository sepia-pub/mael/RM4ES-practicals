"""
Template for scheduler development.
"""

from batsim.batsim import BatsimScheduler, Batsim


class Template(BatsimScheduler):
    def onSimulationBegins(self):
        """Initialize your necessary variables and data structures..."""
        pass

    def onJobSubmission(self, job):
        """Batsim signals that a job arrived. Update the data structs.."""
        pass

    def onJobCompletion(self, job):
        """Batsim signals that a job completed. Update the data structs.."""
        pass

    def onNoMoreEvents(self):
        """Nothing else happens at this time stamp: make the scheduling decisions"""
        self.scheduleJobs()

    def scheduleJobs(self):
        """Make the scheduling decisions"""
        pass